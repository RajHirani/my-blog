import React, { useRef } from 'react';
import { useState } from 'react';

const AddBlog = (props) => {
    const title = useRef(null);
    const blogText = useRef(null);

    const [valid, setValid] = useState(null); 

    const addClickHandler = () =>{
        if(title.current.value && blogText.current.value){
            props.addHandler(title.current.value,blogText.current.value);
            setValid(null);
            title.current.value = "";
            blogText.current.value = "";
        }else{
            setValid(false);
        }
    }

    return(
        <div style={{width:"100%", alignItems:"center"}}>
            <div className="card card-style">
                <div className="card-body">
                 
                    <input className="btn-block" type="text" placeholder="Blog Title" ref={title}></input>
                    
                    <input type="textarea" className="btn-block" placeholder="Blog content" ref={blogText}></input>
                 
                    <button type="button" className="btn btn-primary btn-block" onClick={addClickHandler}>Add</button>

                    {valid==false && <h5 style={{color:"red"}}>Please enter valid values</h5> }
                </div>
            </div>
        </div>
    )
}

export default AddBlog;