import React, { useState } from 'react';
import BlogItem from '../component/blog/BlogItem';
import { doLogout } from '../util/Auth';
import AddBlog from './AddBlog';
import './blog.css';
const Blogs = (props) =>{

    const blogArray = [
        {
            id:1,
            title:"Blog 1",
            text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            id:2,
            title:"Blog 2",
            text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        }
    ];

    const [blogs, setBlogs] = useState(blogArray);


    const removeBlogItems = (id) =>{
        const blogArray = blogs.filter(blog=>{
            return blog.id !== id;
        });
        setBlogs(blogArray);
    }

    const addBlogItems = (title, text) =>{
        let blogArray = [...blogs];
        blogArray.push({ id:Math.random(), title:title, text:text});
        setBlogs(blogArray);
    }

    const updateBlog = (title, text, id) =>{
        const blogArray = blogs.filter(blog=>{
            return blog.id === id;
        });


        blogArray[0].title = title;
        blogArray[0].text = text;

        const newBlogArray = blogs.filter(blog=>{
            return blog.id !== id;
        });
        newBlogArray.push(blogArray[0]);
        setBlogs(newBlogArray);

    }

    const logoutHandler =  () =>{
        doLogout();
        props.history.replace("/login");
    }

    

    const blogItems = blogs.map(blog=>{
        return(
            <BlogItem key={blog.id} title={blog.title} text={blog.text} removeHandler={removeBlogItems} id={blog.id} updateBlog={updateBlog}></BlogItem>
        )
    })

    return(
        <div>
            <div className="blog_header">
                <h1>Blog Page</h1>
                <h5 className="text-primary" onClick={logoutHandler}>logout</h5>
            </div>
            <AddBlog addHandler={addBlogItems}></AddBlog>
            {blogItems}
        </div>
    );
}

export default Blogs;