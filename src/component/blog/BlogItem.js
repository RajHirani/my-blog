import React, { useRef } from 'react';
import { useState } from 'react';

const BlogItem = (props) => {


    const [editMode, setEditMode] = useState(false);
    const [title, setTitle] = useState(props.title);
    const [text, setText] = useState(props.text);

    const removeClickHandler = () =>{
        props.removeHandler(props.id);
    }

    const editClickHandler = () =>{
        setEditMode(true);
    }

    const saveClickHandler = () =>{
        setEditMode(false);
        props.updateBlog(title, text, props.id);
    }

    return(
        <div style={{width:"100%", alignItems:"center"}}>
            <div className="card card-style">
                <div className="card-body">

                    {!editMode && <h5 className="card-title">{props.title}</h5>}
                    {!editMode && <p className="card-text">{props.text}</p>}
                    
                     {editMode && <input className="card-title" value={title} onChange={(event)=>{setTitle(event.target.value);}} placeholder="Blog Title"/>}
                     {editMode && <input type="text" className="card-text" value={text} onChange={(event)=>{setText(event.target.value);}} placeholder="Blog Content"/>}
                     {!editMode && <button type="button" className="btn btn-primary btn-block" onClick={editClickHandler}>Edit</button>}
                     {editMode && <button type="button" className="btn btn-primary btn-block" onClick={saveClickHandler}>Save</button>}
                    <button type="button" className="btn btn-primary btn-block" onClick={removeClickHandler}>Remove</button>
                </div>
            </div>
        </div>
    )
}

export default BlogItem;