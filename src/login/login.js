import React from 'react';
import { doLogin } from '../util/Auth';
import './login.css';

const Login = (props) =>{

    const loginButtonHandler = () =>{
        doLogin();
        props.history.replace("/blog");
    }

    return (
        <div className="container">
            <div className="row">
                <div className="form_bg">
                    
                        <h2 className="text-center">Login Page</h2>
                        <br/>
                        <div className="form-group">
                            <input type="email" className="form-control" placeholder="User id"/>
                        </div>
                        <div className="form-group">
                            <input type="password" className="form-control" placeholder="Password"/>
                        </div>
                        
                        <div className="align-center">
                            <button type="button" className="btn btn-primary btn-block" onClick={loginButtonHandler}>Login</button>
                        </div>
                    
                </div>
            </div>
    </div>
    )
} 

export default Login;