import React from 'react';
import './App.css';
import Login from './login/login';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import PrivateRoute from './component/route/PrivateRoute';
import Blogs from './blog/Blogs';

function App() {

  

  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/login" component={Login}></Route>
          <PrivateRoute Component={Blogs} path="/blog"></PrivateRoute>
          <Redirect from="/" to="/blog"></Redirect>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
