export const isAuthenticated = () =>{
    return localStorage.getItem("auth");
    
}

export const doLogin = () =>{
    localStorage.setItem("auth", true);
}

export const doLogout =() =>{
    localStorage.clear();
}